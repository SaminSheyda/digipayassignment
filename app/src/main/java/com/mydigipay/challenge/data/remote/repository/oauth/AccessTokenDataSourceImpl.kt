package com.mydigipay.challenge.data.remote.repository.oauth

import com.mydigipay.challenge.domain.usecase.network.oauth.AccessTokenService
import com.mydigipay.challenge.domain.usecase.network.oauth.RequestAccessToken

class AccessTokenDataSourceImpl(private val accessTokenService: AccessTokenService) : AccessTokenDataSource {
    override fun accessToken(requestAccessToken: RequestAccessToken) = accessTokenService.accessToken(requestAccessToken)
}