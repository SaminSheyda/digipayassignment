package com.mydigipay.challenge.data.remote.repository.oauth

import com.mydigipay.challenge.domain.usecase.network.oauth.RequestAccessToken
import com.mydigipay.challenge.domain.usecase.network.oauth.ResponseAccessToken
import kotlinx.coroutines.Deferred

interface AccessTokenDataSource {
    fun accessToken(requestAccessToken: RequestAccessToken): Deferred<ResponseAccessToken>
}