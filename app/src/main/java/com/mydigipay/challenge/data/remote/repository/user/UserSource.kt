package com.mydigipay.challenge.data.remote.repository.user

import com.mydigipay.challenge.domain.model.Follower
import com.mydigipay.challenge.domain.model.User
import io.reactivex.Single
import retrofit2.Response

interface UserSource {

    fun getUserProfile(userId: String)
            : Single<Response<User>>

    fun getFollowers(userId: String)
            : Single<Response<List<Follower>>>

    fun getFollowings(userId: String)
            : Single<Response<List<Follower>>>
}