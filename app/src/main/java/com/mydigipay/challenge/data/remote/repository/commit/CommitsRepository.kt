package com.mydigipay.challenge.data.remote.repository.commit


import androidx.lifecycle.LiveData
import com.mydigipay.challenge.domain.model.CommitResponse


interface CommitsRepository{

    fun getCommits(fullname:String): LiveData<List<CommitResponse>>

    fun dispose()
}