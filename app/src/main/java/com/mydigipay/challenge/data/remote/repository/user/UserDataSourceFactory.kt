package com.mydigipay.challenge.data.remote.repository.user

import android.content.Context
import com.mydigipay.challenge.data.remote.repository.Api
import javax.inject.Inject

class UserDataSourceFactory
@Inject constructor(private val iGithubService: Api) : UserSource {

    override fun getUserProfile(userId: String) = iGithubService.getUserProfile(userId)

    override fun getFollowers(userId: String) = iGithubService.getFollowers(userId)

    override fun getFollowings(userId: String) = iGithubService.getFollowings(userId)
}