package com.mydigipay.challenge.data.remote.repository


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.mydigipay.challenge.domain.model.CommitResponse
import com.mydigipay.challenge.domain.model.Follower
import com.mydigipay.challenge.domain.model.RepoSearchResponse
import com.mydigipay.challenge.domain.model.User
import com.mydigipay.challenge.github.BuildConfig
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {
    companion object {
        val BASE_URL="https://api.github.com/"

        val IN_QUALIFIER = "in:name,description"

        fun create():Api{
            val builder = OkHttpClient.Builder()
            if (BuildConfig.DEBUG){
                val loggingInterceptor= HttpLoggingInterceptor()
                loggingInterceptor.level=HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(loggingInterceptor)
            }
            val retrofit: Retrofit =Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .client(builder.build())
                .build()

            return retrofit.create(Api::class.java)
        }
    }

    @GET("search/repositories?sort=stars")
    fun searchRepos(@Query("q") query: String,
                    @Query("page") page: Int,
                    @Query("per_page") itemsPerPage: Int): Observable<RepoSearchResponse>

    @GET("/repos/{owner}/{repo}/commits")
    fun getCommits(@Path("owner") owner: String,@Path("repo") repo: String): Single<List<CommitResponse>>


    @GET("users/{login_id}")
    fun getUserProfile(@Path("login_id") loginId: String): Single<Response<User>>

    @GET("users/{userId}/followers")
    fun getFollowers(@Path("userId") userId: String):  Single<Response<List<Follower>>>

    @GET("users/{userId}/following")
    fun getFollowings(@Path("userId") userId: String):  Single<Response<List<Follower>>>

}