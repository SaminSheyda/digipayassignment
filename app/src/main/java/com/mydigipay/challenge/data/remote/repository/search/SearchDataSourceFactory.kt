package com.mydigipay.challenge.data.remote.repository.search

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.mydigipay.challenge.data.remote.repository.Api
import com.mydigipay.challenge.domain.model.Repo
import io.reactivex.disposables.CompositeDisposable

class SearchDataSourceFactory(val api: Api, val compositeDisposable: CompositeDisposable, val query:String):
    DataSource.Factory<Int, Repo>() {
    val searchLiveDataSource:MutableLiveData<PageKeyedDataSource<Int, Repo>> = MutableLiveData()


    override fun create(): DataSource<Int, Repo> {
        val searchDataSource= SearchDataSource(api, compositeDisposable, query)

        searchLiveDataSource.postValue(searchDataSource)

        return searchDataSource
    }


}