package com.mydigipay.challenge.data.remote.repository.commit

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mydigipay.challenge.data.remote.repository.Api
import com.mydigipay.challenge.domain.model.CommitResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class RepositoryImpl(private val commitService: Api) : CommitsRepository {

    private val compositeDisposable = CompositeDisposable()
    private val commitsLiveData = MutableLiveData<List<CommitResponse>>()

    override fun getCommits(fullname: String): LiveData<List<CommitResponse>> {

        val parts = fullname.split("/")
        compositeDisposable.add(commitService.getCommits(parts[0],parts[1])
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io()).subscribe(
                {
                    commitsLiveData.value = it
                },
                {
                    handleError(it)

                }
            ))

        return commitsLiveData
    }

    override fun dispose() {

        compositeDisposable.clear()
    }
    private fun handleError(throwable:Throwable){
        if (throwable.message != null){
            throwable.printStackTrace()
        }
    }
}