package com.mydigipay.challenge.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import com.mydigipay.challenge.domain.model.User;



public class UserLocalStore {
    public static final String SP_NAME="userDetails";
    SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context)
    {
        userLocalDatabase=context.getSharedPreferences(SP_NAME,0);
    }
    public void storeUserData(String name,String id, String email,String uri, boolean loggedIn,String token){
        SharedPreferences.Editor spEfitor=userLocalDatabase.edit();
        spEfitor.putString("name",name);
        spEfitor.putString("id",id);
        spEfitor.putBoolean("loggedIn", loggedIn);
        spEfitor.putString ("email",email);
        spEfitor.putString ("token",token);
        spEfitor.putString ("Img_uri",uri);
        spEfitor.commit();
    }


    public String getCurrentUser()
    {
        String user = userLocalDatabase.getString("name", "");
        return user;
    }

    public String getToken()
    {
        String token = userLocalDatabase.getString("token", "");
        return token;
    }
    public void clearUserData()
    {
        SharedPreferences.Editor spEfitor=userLocalDatabase.edit();
        spEfitor.clear();
        spEfitor.commit();
    }

    public boolean getUserLoggedIn()
    {
        Boolean loggedin;
         loggedin=userLocalDatabase.getBoolean("loggedIn",false);
        return  loggedin;
    }

}