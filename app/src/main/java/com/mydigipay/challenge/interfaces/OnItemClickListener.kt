package com.mydigipay.challenge.interfaces

import android.view.View
import com.mydigipay.challenge.domain.model.Repo

interface OnItemClickListener {
    fun onItemClick(item: Repo?, view: View?, possition: Int)
}