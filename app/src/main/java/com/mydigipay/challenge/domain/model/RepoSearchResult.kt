package com.mydigipay.challenge.domain.model

import androidx.lifecycle.LiveData
import androidx.paging.PagedList


data class RepoSearchResult(
    val data: LiveData<PagedList<Repo>>
)