package com.mydigipay.challenge.domain.usecase.network.di

import com.mydigipay.challenge.domain.usecase.network.oauth.AccessTokenService
import com.mydigipay.challenge.data.remote.repository.oauth.AccessTokenDataSource
import com.mydigipay.challenge.data.remote.repository.oauth.AccessTokenDataSourceImpl
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val accessTokenModule = module {
    factory { get<Retrofit>(named(RETROFIT)).create(AccessTokenService::class.java) }
    factory { AccessTokenDataSourceImpl(get()) as AccessTokenDataSource }
}