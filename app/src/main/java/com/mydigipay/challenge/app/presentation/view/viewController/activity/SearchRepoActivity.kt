package com.mydigipay.challenge.app.presentation.view.viewController.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnTextChanged
import com.mydigipay.challenge.app.presentation.view.viewController.adapter.SearchAdapter
import com.mydigipay.challenge.app.presentation.viewModel.SearchViewModel
import com.mydigipay.challenge.github.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search_repo.*
import java.util.concurrent.TimeUnit
import com.jakewharton.rxbinding2.widget.RxTextView
import com.mydigipay.challenge.app.presentation.view.viewController.fragment.CommitsFragment
import com.mydigipay.challenge.app.presentation.view.viewController.fragment.ProfileFragment
import com.mydigipay.challenge.domain.model.Repo
import com.mydigipay.challenge.interfaces.OnItemClickListener

class SearchRepoActivity : AppCompatActivity(), OnItemClickListener {

    private lateinit var adapter: SearchAdapter
    private val compositeDisposable = CompositeDisposable()
    private lateinit var searchViewModel: SearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_repo)
        ButterKnife.bind(this)

        searchViewModel = ViewModelProviders.of(this)
            .get(SearchViewModel::class.java)
        adapter = SearchAdapter(this)


        list.layoutManager = LinearLayoutManager(this)
        list.adapter = adapter


        initAdapter()

        profile.setOnClickListener {
            OpenFragment(ProfileFragment())
        }

    }

    @OnTextChanged(R.id.search_repo,callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected fun initSearch() {

        compositeDisposable.add(RxTextView.textChangeEvents(search_repo)
            .skipInitialValue()
            .debounce(1000, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ t ->
                if (t!!.text().isEmpty()) {
                    list.visibility = View.GONE
                    enterToSearch.visibility = View.VISIBLE
                } else {
                    updateRepoListFromInput()

                }
            }) {
                Log.e("TAG", it.printStackTrace().toString())
            })
    }



    private fun updateRepoListFromInput() {


        //start new search
        search_repo.text!!.trim().let {
            if (it.isNotEmpty()) {
                list.scrollToPosition(0)
                searchViewModel.searchRepos(it.toString())
                adapter.submitList(null)
            }
        }
    }

    private fun initAdapter() {

        searchViewModel.repos!!.observe(this, Observer {
            showEmptyList(it?.size==0)
            adapter.submitList(it!!)
        })
    }

    private fun showEmptyList(b:Boolean){
        if(b){
            list.visibility = View.GONE
            emptyList.visibility = View.VISIBLE
        }else{
            list.visibility = View.VISIBLE
            enterToSearch.visibility = View.GONE
            emptyList.visibility = View.GONE
        }
    }

    companion object {
        val LAST_SEARCH="last_search"
        private const val DEFAULT_QUERY = "Android"

    }


    private fun OpenFragment(fragment: DialogFragment) {
        val fm = supportFragmentManager
//        val bundle = Bundle()
//        bundle.putSerializable("item", item)
//        fragment.arguments = bundle
        fragment.show(fm, "DetailFragment")
    }


    override fun onItemClick(item: Repo?, view: View?, position: Int) {
        Log.i("aaaaaa","here2 "+item!!.id.toString())
        searchViewModel.selectedRepo.value=item
        Log.i("aaaaaa","here2 "+searchViewModel.selectedRepo.value!!.id.toString())
        OpenFragment(CommitsFragment())
    }
}
