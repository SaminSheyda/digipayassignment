package com.mydigipay.challenge.app.presentation.view.viewController.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mydigipay.challenge.domain.model.Follower
import com.mydigipay.challenge.github.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_user.view.*


class FollowerAdapter (val context: Context, private  var userList: ArrayList<Follower>):
    RecyclerView.Adapter<FollowerAdapter.FollowerViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FollowerViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_user, p0, false)
        return FollowerViewHolder(view)
    }

    override fun getItemCount() = userList.size

    override fun onBindViewHolder(holder: FollowerViewHolder, position: Int) {
        Picasso.get().load(userList[position].avatarUrl)
            .into(holder.ivUserAvatar)

        holder.tvUserName.text = userList[position].login
    }


    class FollowerViewHolder(view: View): RecyclerView.ViewHolder(view){
        val ivUserAvatar = view.civUserAvatar!!
        val tvUserName = view.tvLoginId!!
    }
}