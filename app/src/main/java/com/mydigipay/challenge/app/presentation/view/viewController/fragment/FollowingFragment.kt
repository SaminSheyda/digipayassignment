package com.mydigipay.challenge.app.presentation.view.viewController.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.mydigipay.challenge.app.presentation.view.viewController.activity.MainActivity
import com.mydigipay.challenge.app.presentation.view.viewController.activity.SearchRepoActivity
import com.mydigipay.challenge.app.presentation.view.viewController.adapter.FollowerAdapter
import com.mydigipay.challenge.app.presentation.viewModel.ProfileViewModel
import com.mydigipay.challenge.domain.model.Follower
import com.mydigipay.challenge.domain.model.SearchedUser

import com.mydigipay.challenge.github.R
import kotlinx.android.synthetic.main.fragment_followers.*

/**
 * A simple [Fragment] subclass.
 */
class FollowingFragment : Fragment() {

    private lateinit var followingViewModel: ProfileViewModel


    private lateinit var followingListAdapter: FollowerAdapter
    private lateinit var layoutManager1: LinearLayoutManager
    private lateinit var followingList: ArrayList<Follower>

    private lateinit var currentUser: String

    companion object {
        private const val CURRENT_USER = "current_user"

        fun newInstance(currentUser: String): FollowingFragment {
            val args: Bundle = Bundle()
            args.putString(CURRENT_USER, currentUser)
            val fragment = FollowingFragment()
            fragment.arguments = args
            return fragment
        }
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followers, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        followingViewModel = ViewModelProviders.of(this@FollowingFragment)
            .get(ProfileViewModel::class.java)

        currentUser = arguments?.getString(CURRENT_USER)!!

        followingList = ArrayList()
        observeFollowingList()
        followingViewModel.getFollowing(currentUser)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        followingListAdapter = FollowerAdapter(activity as SearchRepoActivity, followingList)

        layoutManager1 = LinearLayoutManager(activity as SearchRepoActivity)

        rvFollowers.apply {
            layoutManager = layoutManager1
            adapter = followingListAdapter
        }
    }

    private fun observeFollowingList() {
        followingViewModel.followingList.observe(this, Observer {
            followingList.clear()
            followingList.addAll(it!!)
            followingListAdapter.notifyDataSetChanged()
        })
    }

}
