package com.mydigipay.challenge.app.presentation.view.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.ColorInt;
import androidx.annotation.Dimension;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import com.mydigipay.challenge.github.R;


public class CircleImageView extends AppCompatImageView {

    private static final int DEF_PRESS_HIGHLIGHT_COLOR = 0x32000000;

    private Shader mBitmapShader;
    private Matrix mShaderMatrix;

    private RectF mBitmapDrawBounds;
    private RectF mStrokeBounds;

    private Bitmap mBitmap;

    private Paint mBitmapPaint;
    private Paint mStrokePaint;
    private Paint mPressedPaint;

    private boolean mInitialized;
    private boolean mPressed;
    private boolean mHighlightEnable;

    public CircleImageView(Context context) {
        this(context, null);
    }

    public CircleImageView(Context context, @Nullable AttributeSet attrs) {

        super(context, attrs);

        int strokeColor = Color.TRANSPARENT;
        float strokeWidth = 0;
        boolean highlightEnable = true;
        int highlightColor = DEF_PRESS_HIGHLIGHT_COLOR;

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleImageView, 0, 0);

            strokeColor = a.getColor(R.styleable.CircleImageView_strokeColor, Color.TRANSPARENT);
            strokeWidth = a.getDimensionPixelSize(R.styleable.CircleImageView_strokeWidth, 0);
            highlightEnable = a.getBoolean(R.styleable.CircleImageView_highlightEnable, true);
            highlightColor = a.getColor(R.styleable.CircleImageView_highlightColor, DEF_PRESS_HIGHLIGHT_COLOR);

            a.recycle();
        }

        mShaderMatrix = new Matrix();
        mBitmapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mStrokeBounds = new RectF();
        mBitmapDrawBounds = new RectF();
        mStrokePaint.setColor(strokeColor);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeWidth(strokeWidth);

        mPressedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPressedPaint.setColor(highlightColor);
        mPressedPaint.setStyle(Paint.Style.FILL);

        mHighlightEnable = highlightEnable;
        mInitialized = true;

        setupBitmap();
    }

    @Override
    public void setImageResource(@DrawableRes int resId) {
        super.setImageResource(resId);
        setupBitmap();
    }

    @Override
    public void setImageDrawable(@Nullable Drawable drawable) {
        super.setImageDrawable(drawable);
        setupBitmap();
    }

    @Override
    public void setImageBitmap(@Nullable Bitmap bm) {
        super.setImageBitmap(bm);
        setupBitmap();
    }

    @Override
    public void setImageURI(@Nullable Uri uri) {
        super.setImageURI(uri);
        setupBitmap();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        float halfStrokeWidth = mStrokePaint.getStrokeWidth() / 2f;
        updateCircleDrawBounds(mBitmapDrawBounds);
        mStrokeBounds.set(mBitmapDrawBounds);
        mStrokeBounds.inset(halfStrokeWidth, halfStrokeWidth);

        updateBitmapSize();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean processed = false;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (!isInCircle(event.getX(), event.getY())) {
                    return false;
                }
                processed = true;
                mPressed = true;
                invalidate();
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                processed = true;
                mPressed = false;
                invalidate();
                if (!isInCircle(event.getX(), event.getY())) {
                    return false;
                }
                break;
        }
        return super.onTouchEvent(event) || processed;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawBitmap(canvas);
        drawStroke(canvas);
        drawHighlight(canvas);
    }

    public boolean isHighlightEnable() {
        return mHighlightEnable;
    }

    public void setHighlightEnable(boolean enable) {
        mHighlightEnable = enable;
        invalidate();
    }

    @ColorInt
    public int getHighlightColor() {
        return mPressedPaint.getColor();
    }

    public void setHighlightColor(@ColorInt int color) {
        mPressedPaint.setColor(color);
        invalidate();
    }

    @ColorInt
    public int getStrokeColor() {
        return mStrokePaint.getColor();
    }

    public void setStrokeColor(@ColorInt int color) {
        mStrokePaint.setColor(color);
        invalidate();
    }

    @Dimension
    public float getStrokeWidth() {
        return mStrokePaint.getStrokeWidth();
    }

    public void setStrokeWidth(@Dimension float width) {
        mStrokePaint.setStrokeWidth(width);
        invalidate();
    }

    protected void drawHighlight(Canvas canvas) {
        if (mHighlightEnable && mPressed) {
            canvas.drawOval(mBitmapDrawBounds, mPressedPaint);
        }
    }

    protected void drawStroke(Canvas canvas) {
        if (mStrokePaint.getStrokeWidth() > 0f) {
            canvas.drawOval(mStrokeBounds, mStrokePaint);
        }
    }

    protected void drawBitmap(Canvas canvas) {
        canvas.drawOval(mBitmapDrawBounds, mBitmapPaint);
    }

    protected void updateCircleDrawBounds(RectF bounds) {
        float contentWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        float contentHeight = getHeight() - getPaddingTop() - getPaddingBottom();

        float left = getPaddingLeft();
        float top = getPaddingTop();
        if (contentWidth > contentHeight) {
            left += (contentWidth - contentHeight) / 2f;
        } else {
            top += (contentHeight - contentWidth) / 2f;
        }

        float diameter = Math.min(contentWidth, contentHeight);
        bounds.set(left, top, left + diameter, top + diameter);
    }

    private void setupBitmap() {
        if (!mInitialized) {
            return;
        }
        mBitmap = getBitmapFromDrawable(getDrawable());
        if (mBitmap == null) {
            return;
        }

        mBitmapShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        mBitmapPaint.setShader(mBitmapShader);

        updateBitmapSize();
    }

    private void updateBitmapSize() {
        if (mBitmap == null) return;

        float dx;
        float dy;
        float scale;

        // scale up/down with respect to this view size and maintain aspect ratio
        // translate bitmap position with dx/dy to the center of the image
        if (mBitmap.getWidth() < mBitmap.getHeight()) {
            scale = mBitmapDrawBounds.width() / (float)mBitmap.getWidth();
            dx = mBitmapDrawBounds.left;
            dy = mBitmapDrawBounds.top - (mBitmap.getHeight() * scale / 2f) + (mBitmapDrawBounds.width() / 2f);
        } else {
            scale = mBitmapDrawBounds.height() / (float)mBitmap.getHeight();
            dx = mBitmapDrawBounds.left - (mBitmap.getWidth() * scale / 2f) + (mBitmapDrawBounds.width() / 2f);
            dy = mBitmapDrawBounds.top;
        }
        mShaderMatrix.setScale(scale, scale);
        mShaderMatrix.postTranslate(dx, dy);
        mBitmapShader.setLocalMatrix(mShaderMatrix);
    }

    private Bitmap getBitmapFromDrawable(Drawable drawable) {
        if (drawable == null) {
            return null;
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(
                drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private boolean isInCircle(float x, float y) {
        // find the distance between center of the view and x,y point
        double distance = Math.sqrt(
                Math.pow(mBitmapDrawBounds.centerX() - x, 2) + Math.pow(mBitmapDrawBounds.centerY() - y, 2)
        );
        return distance <= (mBitmapDrawBounds.width() / 2);
    }
}

//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.graphics.Bitmap;
//import android.graphics.BitmapShader;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.ColorFilter;
//import android.graphics.Matrix;
//import android.graphics.Outline;
//import android.graphics.Paint;
//import android.graphics.Rect;
//import android.graphics.RectF;
//import android.graphics.Shader;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.ColorDrawable;
//import android.graphics.drawable.Drawable;
//import android.net.Uri;
//import android.os.Build;
//import android.util.AttributeSet;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewOutlineProvider;
//import android.widget.ImageView;
//
//import com.coimex.R;
//
//
//@SuppressWarnings("UnusedDeclaration")
//public class CircleImageView extends android.support.v7.widget.AppCompatImageView {
//
//    private static final ScaleType SCALE_TYPE = ScaleType.CENTER_CROP;
//
//    private static final Bitmap.Config BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
//    private static final int COLORDRAWABLE_DIMENSION = 2;
//
//    private static final int DEFAULT_BORDER_WIDTH = 0;
//    private static final int DEFAULT_BORDER_COLOR = Color.BLACK;
//    private static final int DEFAULT_CIRCLE_BACKGROUND_COLOR = Color.TRANSPARENT;
//    private static final boolean DEFAULT_BORDER_OVERLAY = false;
//
//    private final RectF mDrawableRect = new RectF();
//    private final RectF mBorderRect = new RectF();
//
//    private final Matrix mShaderMatrix = new Matrix();
//    private final Paint mBitmapPaint = new Paint();
//    private final Paint mBorderPaint = new Paint();
//    private final Paint mCircleBackgroundPaint = new Paint();
//
//    private int mBorderColor = DEFAULT_BORDER_COLOR;
//    private int mBorderWidth = DEFAULT_BORDER_WIDTH;
//    private int mCircleBackgroundColor = DEFAULT_CIRCLE_BACKGROUND_COLOR;
//
//    private Bitmap mBitmap;
//    private BitmapShader mBitmapShader;
//    private int mBitmapWidth;
//    private int mBitmapHeight;
//
//    private float mDrawableRadius;
//    private float mBorderRadius;
//
//    private ColorFilter mColorFilter;
//
//    private boolean mReady;
//    private boolean mSetupPending;
//    private boolean mBorderOverlay;
//    private boolean mDisableCircularTransformation;
//
//    public CircleImageView(Context context) {
//        super(context);
//
//        init();
//    }
//
//    public CircleImageView(Context context, AttributeSet attrs) {
//        this(context, attrs, 0);
//    }
//
//    public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
//        super(context, attrs, defStyle);
//
//        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleImageView, defStyle, 0);
//
//        mBorderWidth = a.getDimensionPixelSize(R.styleable.CircleImageView_civ_border_width, DEFAULT_BORDER_WIDTH);
//        mBorderColor = a.getColor(R.styleable.CircleImageView_civ_border_color, DEFAULT_BORDER_COLOR);
//        mBorderOverlay = a.getBoolean(R.styleable.CircleImageView_civ_border_overlay, DEFAULT_BORDER_OVERLAY);
//        mCircleBackgroundColor = a.getColor(R.styleable.CircleImageView_civ_circle_background_color, DEFAULT_CIRCLE_BACKGROUND_COLOR);
//
//        a.recycle();
//
//        init();
//    }
//
//    private void init() {
//        super.setScaleType(SCALE_TYPE);
//        mReady = true;
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            setOutlineProvider(new OutlineProvider());
//        }
//
//        if (mSetupPending) {
//            setup();
//            mSetupPending = false;
//        }
//    }
//
//    @Override
//    public ScaleType getScaleType() {
//        return SCALE_TYPE;
//    }
//
//    @Override
//    public void setScaleType(ScaleType scaleType) {
//        if (scaleType != SCALE_TYPE) {
//            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
//        }
//    }
//
//    @Override
//    public void setAdjustViewBounds(boolean adjustViewBounds) {
//        if (adjustViewBounds) {
//            throw new IllegalArgumentException("adjustViewBounds not supported.");
//        }
//    }
//
//    @Override
//    protected void onDraw(Canvas canvas) {
//        if (mDisableCircularTransformation) {
//            super.onDraw(canvas);
//            return;
//        }
//
//        if (mBitmap == null) {
//            return;
//        }
//
//        if (mCircleBackgroundColor != Color.TRANSPARENT) {
//            canvas.drawCircle(mDrawableRect.centerX(), mDrawableRect.centerY(), mDrawableRadius, mCircleBackgroundPaint);
//        }
//        canvas.drawCircle(mDrawableRect.centerX(), mDrawableRect.centerY(), mDrawableRadius, mBitmapPaint);
//        if (mBorderWidth > 0) {
//            canvas.drawCircle(mBorderRect.centerX(), mBorderRect.centerY(), mBorderRadius, mBorderPaint);
//        }
//    }
//
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//        setup();
//    }
//
//    @Override
//    public void setPadding(int left, int top, int right, int bottom) {
//        super.setPadding(left, top, right, bottom);
//        setup();
//    }
//
//    @Override
//    public void setPaddingRelative(int start, int top, int end, int bottom) {
//        super.setPaddingRelative(start, top, end, bottom);
//        setup();
//    }
//
//    public int getBorderColor() {
//        return mBorderColor;
//    }
//
//    public void setBorderColor(@ColorInt int borderColor) {
//        if (borderColor == mBorderColor) {
//            return;
//        }
//
//        mBorderColor = borderColor;
//        mBorderPaint.setColor(mBorderColor);
//        invalidate();
//    }
//
//    public int getCircleBackgroundColor() {
//        return mCircleBackgroundColor;
//    }
//
//    public void setCircleBackgroundColor(@ColorInt int circleBackgroundColor) {
//        if (circleBackgroundColor == mCircleBackgroundColor) {
//            return;
//        }
//
//        mCircleBackgroundColor = circleBackgroundColor;
//        mCircleBackgroundPaint.setColor(circleBackgroundColor);
//        invalidate();
//    }
//
//    public void setCircleBackgroundColorResource(@ColorRes int circleBackgroundRes) {
//        setCircleBackgroundColor(getContext().getResources().getColor(circleBackgroundRes));
//    }
//
//    public int getBorderWidth() {
//        return mBorderWidth;
//    }
//
//    public void setBorderWidth(int borderWidth) {
//        if (borderWidth == mBorderWidth) {
//            return;
//        }
//
//        mBorderWidth = borderWidth;
//        setup();
//    }
//
//    public boolean isBorderOverlay() {
//        return mBorderOverlay;
//    }
//
//    public void setBorderOverlay(boolean borderOverlay) {
//        if (borderOverlay == mBorderOverlay) {
//            return;
//        }
//
//        mBorderOverlay = borderOverlay;
//        setup();
//    }
//
//    public boolean isDisableCircularTransformation() {
//        return mDisableCircularTransformation;
//    }
//
//    public void setDisableCircularTransformation(boolean disableCircularTransformation) {
//        if (mDisableCircularTransformation == disableCircularTransformation) {
//            return;
//        }
//
//        mDisableCircularTransformation = disableCircularTransformation;
//        initializeBitmap();
//    }
//
//    @Override
//    public void setImageBitmap(Bitmap bm) {
//        super.setImageBitmap(bm);
//        initializeBitmap();
//    }
//
//    @Override
//    public void setImageDrawable(Drawable drawable) {
//        super.setImageDrawable(drawable);
//        initializeBitmap();
//    }
//
//    @Override
//    public void setImageResource(@DrawableRes int resId) {
//        super.setImageResource(resId);
//        initializeBitmap();
//    }
//
//    @Override
//    public void setImageURI(Uri uri) {
//        super.setImageURI(uri);
//        initializeBitmap();
//    }
//
//    @Override
//    public void setColorFilter(ColorFilter cf) {
//        if (cf == mColorFilter) {
//            return;
//        }
//
//        mColorFilter = cf;
//        applyColorFilter();
//        invalidate();
//    }
//
//    @Override
//    public ColorFilter getColorFilter() {
//        return mColorFilter;
//    }
//
//    private void applyColorFilter() {
//        mBitmapPaint.setColorFilter(mColorFilter);
//    }
//
//    private Bitmap getBitmapFromDrawable(Drawable drawable) {
//        if (drawable == null) {
//            return null;
//        }
//
//        if (drawable instanceof BitmapDrawable) {
//            return ((BitmapDrawable) drawable).getBitmap();
//        }
//
//        try {
//            Bitmap bitmap;
//
//            if (drawable instanceof ColorDrawable) {
//                bitmap = Bitmap.createBitmap(COLORDRAWABLE_DIMENSION, COLORDRAWABLE_DIMENSION, BITMAP_CONFIG);
//            } else {
//                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), BITMAP_CONFIG);
//            }
//
//            Canvas canvas = new Canvas(bitmap);
//            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//            drawable.draw(canvas);
//            return bitmap;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    private void initializeBitmap() {
//        if (mDisableCircularTransformation) {
//            mBitmap = null;
//        } else {
//            mBitmap = getBitmapFromDrawable(getDrawable());
//        }
//        setup();
//    }
//
//    private void setup() {
//        if (!mReady) {
//            mSetupPending = true;
//            return;
//        }
//
//        if (getWidth() == 0 && getHeight() == 0) {
//            return;
//        }
//
//        if (mBitmap == null) {
//            invalidate();
//            return;
//        }
//
//        mBitmapShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
//
//        mBitmapPaint.setAntiAlias(true);
//        mBitmapPaint.setShader(mBitmapShader);
//
//        mBorderPaint.setStyle(Paint.Style.STROKE);
//        mBorderPaint.setAntiAlias(true);
//        mBorderPaint.setColor(mBorderColor);
//        mBorderPaint.setStrokeWidth(mBorderWidth);
//
//        mCircleBackgroundPaint.setStyle(Paint.Style.FILL);
//        mCircleBackgroundPaint.setAntiAlias(true);
//        mCircleBackgroundPaint.setColor(mCircleBackgroundColor);
//
//        mBitmapHeight = mBitmap.getHeight();
//        mBitmapWidth = mBitmap.getWidth();
//
//        mBorderRect.set(calculateBounds());
//        mBorderRadius = Math.min((mBorderRect.height() - mBorderWidth) / 2.0f, (mBorderRect.width() - mBorderWidth) / 2.0f);
//
//        mDrawableRect.set(mBorderRect);
//        if (!mBorderOverlay && mBorderWidth > 0) {
//            mDrawableRect.inset(mBorderWidth - 1.0f, mBorderWidth - 1.0f);
//        }
//        mDrawableRadius = Math.min(mDrawableRect.height() / 2.0f, mDrawableRect.width() / 2.0f);
//
//        applyColorFilter();
//        updateShaderMatrix();
//        invalidate();
//    }
//
//    private RectF calculateBounds() {
//        int availableWidth  = getWidth() - getPaddingLeft() - getPaddingRight();
//        int availableHeight = getHeight() - getPaddingTop() - getPaddingBottom();
//
//        int sideLength = Math.min(availableWidth, availableHeight);
//
//        float left = getPaddingLeft() + (availableWidth - sideLength) / 2f;
//        float top = getPaddingTop() + (availableHeight - sideLength) / 2f;
//
//        return new RectF(left, top, left + sideLength, top + sideLength);
//    }
//
//    private void updateShaderMatrix() {
//        float scale;
//        float dx = 0;
//        float dy = 0;
//
//        mShaderMatrix.set(null);
//
//        if (mBitmapWidth * mDrawableRect.height() > mDrawableRect.width() * mBitmapHeight) {
//            scale = mDrawableRect.height() / (float) mBitmapHeight;
//            dx = (mDrawableRect.width() - mBitmapWidth * scale) * 0.5f;
//        } else {
//            scale = mDrawableRect.width() / (float) mBitmapWidth;
//            dy = (mDrawableRect.height() - mBitmapHeight * scale) * 0.5f;
//        }
//
//        mShaderMatrix.setScale(scale, scale);
//        mShaderMatrix.postTranslate((int) (dx + 0.5f) + mDrawableRect.left, (int) (dy + 0.5f) + mDrawableRect.top);
//
//        mBitmapShader.setLocalMatrix(mShaderMatrix);
//    }
//
//    @SuppressLint("ClickableViewAccessibility")
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        return inTouchableArea(event.getX(), event.getY()) && super.onTouchEvent(event);
//    }
//
//    private boolean inTouchableArea(float x, float y) {
//        return Math.pow(x - mBorderRect.centerX(), 2) + Math.pow(y - mBorderRect.centerY(), 2) <= Math.pow(mBorderRadius, 2);
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    private class OutlineProvider extends ViewOutlineProvider {
//
//        @Override
//        public void getOutline(View view, Outline outline) {
//            Rect bounds = new Rect();
//            mBorderRect.roundOut(bounds);
//            outline.setRoundRect(bounds, bounds.width() / 2.0f);
//        }
//
//    }
//
//}