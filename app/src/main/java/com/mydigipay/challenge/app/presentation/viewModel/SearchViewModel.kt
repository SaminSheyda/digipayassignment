package com.mydigipay.challenge.app.presentation.viewModel

import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.mydigipay.challenge.data.remote.repository.Api
import com.mydigipay.challenge.data.remote.repository.commit.CommitsRepository
import com.mydigipay.challenge.data.remote.repository.commit.RepositoryImpl
import com.mydigipay.challenge.data.remote.repository.search.SearchDataSource
import com.mydigipay.challenge.data.remote.repository.search.SearchDataSourceFactory
import com.mydigipay.challenge.domain.model.CommitResponse
import com.mydigipay.challenge.domain.model.Repo
import com.mydigipay.challenge.domain.model.RepoSearchResult
import io.reactivex.disposables.CompositeDisposable


class SearchViewModel: ViewModel(){
    var searchPagedList: LiveData<PagedList<Repo>>?= null
    var searchDataSource: LiveData<PageKeyedDataSource<Int, Repo>>? = null
    var selectedRepo:MutableLiveData<Repo> = MutableLiveData()

    //store search query
    private val queryLiveData = MutableLiveData<String>()


    //store search result
    private val repoResult: LiveData<RepoSearchResult> = Transformations.map(queryLiveData) {
        initSearch(it)
    }

    //cache search result
    val repos: LiveData<PagedList<Repo>> = Transformations.switchMap(repoResult
    ) { it -> it.data }


    val compositeDisposable= CompositeDisposable()

    fun initSearch(query: String): RepoSearchResult{
        val searchViewModelFactory= SearchDataSourceFactory(Api.create(),compositeDisposable,query)

        searchDataSource=searchViewModelFactory.searchLiveDataSource

        val config=PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(SearchDataSource.PAGE_SIZE)
            .setInitialLoadSizeHint(SearchDataSource.PAGE_SIZE*2)
            .build()

        //Building the paged list
        searchPagedList = LivePagedListBuilder<Int,Repo>(searchViewModelFactory,config)
            .setInitialLoadKey(0)
            .build()
        //return result
        return RepoSearchResult(searchPagedList!!)
    }



    //set new search
    fun searchRepos(query: String){
        queryLiveData.postValue(query)
    }

    //get last query
    fun lastQueryValue(): String? = queryLiveData.value


    fun replaceSubscription(lifecycleOwner: LifecycleOwner, query: String){
        searchPagedList!!.removeObservers(lifecycleOwner)
        initSearch(query)
    }

    var commitsRepository: CommitsRepository? = RepositoryImpl(Api.create())


    fun fetchCommits():LiveData<List<CommitResponse>> =
        commitsRepository!!.getCommits(selectedRepo.value!!.fullName)

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}