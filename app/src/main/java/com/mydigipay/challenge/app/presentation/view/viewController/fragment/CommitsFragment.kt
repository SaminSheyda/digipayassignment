package com.mydigipay.challenge.app.presentation.view.viewController.fragment


import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import com.mydigipay.challenge.app.presentation.view.viewController.adapter.CommitListAdapter
import com.mydigipay.challenge.app.presentation.viewModel.SearchViewModel
import com.mydigipay.challenge.domain.model.CommitResponse
import com.mydigipay.challenge.github.R
import kotlinx.android.synthetic.main.fragment_commits.*
import kotlinx.android.synthetic.main.fragment_commits.view.*


/**
 * A simple [Fragment] subclass.
 */
    class CommitsFragment : DialogFragment() {

    private lateinit var searchViewModel: SearchViewModel
    private lateinit var commitListAdapter: CommitListAdapter
    private var commits = listOf<CommitResponse>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView: View=inflater.inflate(R.layout.fragment_commits, container, false)
        searchViewModel = ViewModelProviders.of(activity!!)
            .get(SearchViewModel::class.java)

        ButterKnife.bind(this,rootView)

        Log.i("aaaaa","here  "+searchViewModel.selectedRepo.value?.fullName.toString())

        commitListAdapter = CommitListAdapter()
        rootView.rvCommits.adapter=commitListAdapter
        rootView.rvCommits.layoutManager=LinearLayoutManager(activity)

        fetchdata()


        rootView.img_commits_back.setOnClickListener {
            this.dismiss()
        }
        // Inflate the layout for this fragment
        return rootView
    }
    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        return object : Dialog(activity!!, theme) {
            override fun onBackPressed() {

                super.onBackPressed()
            }
        }
    }


   fun fetchdata() {
        super.onResume()
        Log.i("onResume","onResume")
        searchViewModel.fetchCommits().observe(this, Observer {
            Log.i("onResume","onResume  "+it.size)
            commitListAdapter = CommitListAdapter()
            commits=it
            commitListAdapter.setData(commits)

            rvCommits.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = commitListAdapter
                commits = it
            }
        })


    }

}
