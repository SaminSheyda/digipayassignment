package com.mydigipay.challenge.app.presentation.viewModel

import android.service.autofill.UserData
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mydigipay.challenge.data.remote.repository.Api
import com.mydigipay.challenge.data.remote.repository.user.UserDataSourceFactory
import com.mydigipay.challenge.data.remote.repository.user.UserSource
import com.mydigipay.challenge.domain.model.Follower
import com.mydigipay.challenge.domain.model.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class ProfileViewModel: ViewModel()
{

var loading = MutableLiveData<Boolean>()
var message = MutableLiveData<String>()

var userProfile = MutableLiveData<User>()
    var followersList = MutableLiveData<List<Follower>>()
    var followingList = MutableLiveData<List<Follower>>()


    private val compositeDisposable = CompositeDisposable()
var userSource:UserSource?=UserDataSourceFactory(Api.create())


fun getUserProfile(userId: String) {
    loading.value = true
    val searchDisposable = userSource!!.getUserProfile(userId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(object : DisposableSingleObserver<Response<User>>() {
            override fun onSuccess(response: Response<User>) {
                loading.value = false
                if (response.isSuccessful && response.code() == 200) {
                    /*if (response.body()?.userList != null &&
                        (response.body()?.userList as List).isNotEmpty()){
                        searchedUsersList.value = response?.body()?.userList as ArrayList<SearchedUser>
                    }*/

                    userProfile.value = response.body()
                } else {
                    message.value = response.message()
                }
            }

            override fun onError(throwable: Throwable) {
                loading.value = false
                if (throwable.message != null)
                    message.value = throwable.message
                /*else
                    message.value = context.getString(R.string.unable_to_fetch_restaurants)*/
            }
        })
    compositeDisposable.add(searchDisposable)
}

    fun getFollowers(userId: String) {
        loading.value = true
        val searchDisposable = userSource!!.getFollowers(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<Response<List<Follower>>>() {
                override fun onSuccess(response: Response<List<Follower>>) {
                    loading.value = false
                    if (response.isSuccessful && response.code() == 200) {
                        if (response.body() != null &&
                            (response.body() as List).isNotEmpty()){
                            followersList.value = response.body() as ArrayList<Follower>
                        }
                    } else {
                        message.value = response.message()
                    }
                }

                override fun onError(throwable: Throwable) {
                    loading.value = false
                    if (throwable.message != null)
                        message.value = throwable.message
                    /*else
                        message.value = context.getString(R.string.unable_to_fetch_restaurants)*/
                }
            })
        compositeDisposable.add(searchDisposable)
    }

    fun getFollowing(userId: String) {
        loading.value = true
        val searchDisposable = userSource!!.getFollowings(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<Response<List<Follower>>>() {
                override fun onSuccess(response: Response<List<Follower>>) {
                    loading.value = false
                    if (response.isSuccessful && response.code() == 200) {
                        if (response.body() != null &&
                            (response.body() as List).isNotEmpty()){

                            Log.i("jjjjj","here2  "+(response.body() as ArrayList<Follower>).size)
                            followingList.value = response.body() as ArrayList<Follower>
                        }
                    } else {
                        message.value = response.message()
                    }
                }

                override fun onError(throwable: Throwable) {
                    loading.value = false
                    if (throwable.message != null)
                        message.value = throwable.message
                    /*else
                        message.value = context.getString(R.string.unable_to_fetch_restaurants)*/
                }
            })
        compositeDisposable.add(searchDisposable)
    }

    override fun onCleared() {
    super.onCleared()
    compositeDisposable.dispose()
}
}
