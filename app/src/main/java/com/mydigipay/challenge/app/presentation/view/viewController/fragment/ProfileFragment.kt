package com.mydigipay.challenge.app.presentation.view.viewController.fragment


import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mydigipay.challenge.app.presentation.view.viewController.activity.SearchRepoActivity
import com.mydigipay.challenge.app.presentation.view.viewController.adapter.TabAdapter
import com.mydigipay.challenge.app.presentation.viewModel.ProfileViewModel
import com.mydigipay.challenge.data.local.UserLocalStore
import com.mydigipay.challenge.domain.model.SearchedUser

import com.mydigipay.challenge.github.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment :  DialogFragment() {

    private lateinit var userProfileViewModel: ProfileViewModel
     var currentUser: String?=null
    var localStore: UserLocalStore ?= null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
        localStore = UserLocalStore(context)
        currentUser=localStore!!.currentUser

        userProfileViewModel = ViewModelProviders.of(this@ProfileFragment)
            .get(ProfileViewModel::class.java)

        observeLoading()
        observeMessage()
        observeUserProfile()

//        currentUser = arguments?.getParcelable("SEARCHED_USER")!!


//            userProfileViewModel.getUserProfile(currentUser.login)
            userProfileViewModel.getUserProfile(currentUser!!)


    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        return object : Dialog(activity!!, theme) {
            override fun onBackPressed() {

                super.onBackPressed()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivBackArrow.setOnClickListener {
            activity?.supportFragmentManager?.popBackStackImmediate()
        }


        //Share intent for  sharing the profile of selecting user through
        // other system/external applications.
        ivShare.setOnClickListener {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                "Hey check out this Github profile at: https://github.com/" +currentUser
            )
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        }
    }
    private  fun observeLoading(){
        userProfileViewModel.loading.observe(this, Observer {
//            (activity as SearchRepoActivity).setProgressVisibility(it)
        })
    }

    private  fun observeMessage(){
        userProfileViewModel.message.observe(this, Observer {
//            (activity as SearchRepoActivity).showInfoToast(it)
        })
    }

    private fun observeUserProfile() {

        userProfileViewModel.userProfile.observe(this, Observer {

            Picasso.get()
                .load(it.avatarUrl)
                .into(civProfileAvatar)

            tvUserName.text = it.name
            tvLocation.text = it.location
            tvNumberOfGists.text = it.publicGists.toString()
            tvNumberOfRepos.text = it.publicRepos.toString()

            val titleArray = listOf(it.followers.toString(), it.following.toString())
//             Create an adapter that knows which fragment should be shown on each page
//            val adapter = TabAdapter(activity?.supportFragmentManager!!, titleArray, "swankjesse", 1)
            val adapter = TabAdapter(childFragmentManager, titleArray, currentUser!!, 1)

            // Set the adapter onto the view pager
            viewPager.adapter = adapter

            // Give the TabLayout the ViewPager
            tabLayout.setupWithViewPager(viewPager)
        })
    }
}