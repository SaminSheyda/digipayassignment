package com.mydigipay.challenge.app.presentation.view.viewController.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.mydigipay.challenge.app.presentation.view.viewController.fragment.FollowersFragment
import com.mydigipay.challenge.app.presentation.view.viewController.fragment.FollowingFragment
import com.mydigipay.challenge.domain.model.SearchedUser

class TabAdapter(fm: FragmentManager, var titles: List<String>, var searchedUser: String,
                 BEHAVIOR_SET_USER_VISIBLE_HINT: Int) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FollowersFragment.newInstance(searchedUser)
            1 -> FollowingFragment.newInstance(searchedUser)
            else -> FollowersFragment()
        }
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> {
                return "Followers  (${titles[0]})"
            }

            1 -> {
                return "Following  (${titles[1]})"
            }

            else -> "Followers  (${titles[0]})"
        }
    }
}