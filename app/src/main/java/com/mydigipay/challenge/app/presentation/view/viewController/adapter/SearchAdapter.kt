package com.mydigipay.challenge.app.presentation.view.viewController.adapter



import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mydigipay.challenge.domain.model.Repo
import com.mydigipay.challenge.github.R
import com.mydigipay.challenge.interfaces.OnItemClickListener

import kotlinx.android.synthetic.main.repo_view_item.view.*

class SearchAdapter(onItemClickListener: OnItemClickListener): PagedListAdapter<Repo, SearchAdapter.ViewHolder>(REPO_COMPARATOR) {

    var onItemClickListener: OnItemClickListener? = onItemClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.repo_view_item,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(getItem(position))
        holder.itemView.setOnClickListener {
//            getItem(position)?.url?.let { url ->
//                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
//                holder.itemView.context.startActivity(intent)
//            }

            onItemClickListener?.onItemClick(getItem(position),it,position)
        }


    }

    override fun getCurrentList(): PagedList<Repo>? {
        return super.getCurrentList()
    }



    fun clear(){
        if(currentList!!.size>0){

            notifyItemRangeRemoved(0,currentList!!.size-1)
        }

    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindItem(item: Repo?) {
            if (item!=null){
                populateData(item)
            }else{
                val resource=itemView.resources

                itemView.repo_name.text=resource.getString(R.string.loading)
                itemView.repo_description.visibility=View.GONE
                itemView.repo_language.visibility=View.GONE
                itemView.repo_stars.text=resource.getString(R.string.unknown)
                itemView.repo_forks.text=resource.getString(R.string.unknown)
            }

        }

        private fun populateData(item: Repo?) {
            itemView.repo_name.text=item!!.name

            if (item.description!=null){
                itemView.repo_description.text=item!!.description
            }else{
                itemView.repo_description.visibility=View.GONE
            }

            if (item.language!=null){
                itemView.repo_language.text=item!!.language
            }else{
                itemView.repo_language.visibility=View.GONE
            }

            itemView.repo_stars.text=item!!.stars.toString()
            itemView.repo_forks.text=item!!.forks.toString()
        }

    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Repo>() {
            override fun areItemsTheSame(oldItem: Repo, newItem: Repo): Boolean =
                oldItem.fullName == newItem.fullName

            override fun areContentsTheSame(oldItem: Repo, newItem: Repo): Boolean =
                oldItem == newItem
        }
    }
}