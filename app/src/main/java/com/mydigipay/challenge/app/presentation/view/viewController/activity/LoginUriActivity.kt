package com.mydigipay.challenge.app.presentation.view.viewController.activity
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.mydigipay.challenge.data.local.UserLocalStore
import com.mydigipay.challenge.domain.usecase.network.oauth.RequestAccessToken
import com.mydigipay.challenge.data.remote.repository.oauth.AccessTokenDataSource
import com.mydigipay.challenge.data.remote.repository.token.TokenRepository
import com.mydigipay.challenge.domain.model.User
import com.mydigipay.challenge.github.R
import kotlinx.android.synthetic.main.login_uri_activity.*
import kotlinx.coroutines.*
import org.json.JSONObject
import org.json.JSONTokener
import org.koin.android.ext.android.inject
import java.net.URL
import javax.net.ssl.HttpsURLConnection


const val CLIENT_ID = "a632bb167e8ac41d8970"
const val CLIENT_SECRET = "cb3909c2f0bc59e7c13235d27d1d42a4f2cbbc7c"
const val REDIRECT_URI = "challenge://mydigipay.com/login/callback"

class LoginUriActivity : Activity() {
    private val tokenRepository: TokenRepository by inject()
    private val accessTokenDataSource: AccessTokenDataSource by inject()
    var localStore: UserLocalStore ?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i("LoginUriActivity","LoginUriActivity")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_uri_activity)
        localStore = UserLocalStore(this)
        btn_login.setOnClickListener {
            val url = "https://github.com/login/oauth/authorize?client_id=$CLIENT_ID&redirect_uri=$REDIRECT_URI&scope=repo user&state=0"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }

    override fun onResume() {
        super.onResume()
        val intent = intent
//        if (Intent.ACTION_VIEW == intent.action) {
            val uri = intent.data
        if (uri != null && uri.toString().startsWith(REDIRECT_URI)) {
            Log.i("here","IF")
            Log.i("here","IF "+uri)
            val code = uri?.getQueryParameter("code") ?: ""
            code.takeIf { it.isNotEmpty() }?.let { code ->
                val accessTokenJob = CoroutineScope(Dispatchers.IO).launch {
                    val response = accessTokenDataSource.accessToken(
                        RequestAccessToken(
                            CLIENT_ID,
                            CLIENT_SECRET,
                            code,
                            REDIRECT_URI,
                            "0"
                        )
                    ).await()

                    tokenRepository.saveToken(response.accessToken).await()
                    fetchGithubUserProfile(response.accessToken)
                }

                accessTokenJob.invokeOnCompletion {
                    CoroutineScope(Dispatchers.Main).launch {
//                        token.text = tokenRepository.readToken().await()
                        this.cancel()
                        accessTokenJob.cancelAndJoin()
                    }
                }

            } ?: run { finish()

            }
        }

    }



    fun fetchGithubUserProfile(token: String) {
        GlobalScope.launch(Dispatchers.Default) {
            val tokenURLFull =
                "https://api.github.com/user"

            val url = URL(tokenURLFull)
            val httpsURLConnection =
                withContext(Dispatchers.IO) { url.openConnection() as HttpsURLConnection }
            httpsURLConnection.requestMethod = "GET"
            httpsURLConnection.setRequestProperty("Authorization", "Bearer $token")
            httpsURLConnection.doInput = true
            httpsURLConnection.doOutput = false
            val response = httpsURLConnection.inputStream.bufferedReader()
                .use { it.readText() }  // defaults to UTF-8
            val jsonObject = JSONTokener(response).nextValue() as JSONObject
            Log.i("GitHub Access Token: ", token)

            // GitHub Id
            val githubId = jsonObject.getInt("id")
            Log.i("GitHub Id: ", githubId.toString())

            // GitHub Display Name
            val githubDisplayName = jsonObject.getString("login")
            Log.i("GitHub Display Name: ", githubDisplayName)

            // GitHub Email
            val githubEmail = jsonObject.getString("email")
            Log.i("GitHub Email: ", githubEmail)

            // GitHub Profile Avatar URL
            val githubAvatarURL = jsonObject.getString("avatar_url")
            Log.i("Github", githubAvatarURL)
            localStore!!.storeUserData(githubDisplayName,githubId.toString(),githubEmail,githubAvatarURL,true,token)
        }
        val intent = Intent(this, SearchRepoActivity::class.java)
        startActivity(intent)
        this.finish()
    }

}